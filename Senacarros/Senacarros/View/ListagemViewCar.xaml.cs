﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacarros.View
{

    public class Veiculo
    {
        public string Nome_Do_Modelo { get; set; }

        public float Preco_Modelo { get; set; }

        public string Preco_Formatado
        {
            get { return string.Format("R$ {0} ", Preco_Modelo); }
        }

        public string Nome_Da_Marca { get; set; }
    }

    public partial class ListagemViewCar : ContentPage
    {
        public List <Veiculo> veiculos { get; set; }

        public ListagemViewCar()
        {
            InitializeComponent();

            this.veiculos = new List<Veiculo>
            {

                new Veiculo {Nome_Da_Marca = "Ford", Nome_Do_Modelo = "Fiesta",Preco_Modelo = 25000},
                new Veiculo {Nome_Da_Marca = "Chevrolet", Nome_Do_Modelo = "Onix",Preco_Modelo = 35000},
                new Veiculo {Nome_Da_Marca = "Fiat", Nome_Do_Modelo = "GrandSiena",Preco_Modelo = 68000},
                new Veiculo {Nome_Da_Marca = "Volkswagem", Nome_Do_Modelo = "Gol",Preco_Modelo = 52000},
                new Veiculo {Nome_Da_Marca = "Hyundai", Nome_Do_Modelo = "HB20",Preco_Modelo = 220000},

            };

            this.BindingContext = this;
        }

        private void ListViewSenacar_ItemTapped(object sender,ItemTappedEventArgs e)
        {
            var veiculos = (Veiculo)e.Item;

            //Navigation.PushAsync(new Descricao(Veiculo));

        }
    }
   
}
