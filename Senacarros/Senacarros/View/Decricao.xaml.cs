﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacarros.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]

	public partial class Decricao : ContentPage
	{

        private const float VIDRO = 80;
        private const float TRAVAS = 80;
        private const float ARCONDICIONADO = 80;
        private const float CAMERA = 80;


        private const float CAMBIO = 80;
        private const float SUSPENSAO = 80;
        private const float FREIOS = 80;
        




        public string Vidros
        {
            get
            {
                return string.Format("Vidros Elétricos - R$ {0}", VIDRO);
            }
        }

        public string Travas
        {
            get
            {
                return string.Format("Travas Elétricas - R$ {0}", TRAVAS);
            }
        }

        public string ArCondicionado
        {
            get
            {
                return string.Format("Ar Condicionado - R$ {0}", ARCONDICIONADO);
            }
        }

        public string Camera
        {
            get
            {
                return string.Format("Câmera de Ré - R$ {0}", CAMERA);
            }
        }

        public string Cambio
        {
            get
            {
                return string.Format("Câmbio - R$ {0}", CAMBIO);
            }
        }

        public string Suspensao
        {
            get
            {
                return string.Format("Suspensão - R$ {0}", SUSPENSAO);
            }
        }

        public string Freio
        {
            get
            {
                return string.Format("Freios - R$ {0}", FREIOS);
            }
        }
        public string ValorTotal
        {
            get
            {
                return string.Format("Valor total: R$ {0}", Veiculo.Preco_Modelo + (IncluiVidros ? VIDRO : 0) + (IncluiTravas ? TRAVAS : 0)
                    + (IncluiAr ? ARCONDICIONADO : 0) + (IncluiCamera ? CAMERA : 0) + (IncluiCambio ? CAMBIO : 0)
                    + (IncluiSuspensao ? SUSPENSAO : 0) + (IncluiFreios ? FREIOS : 0));

            }

        }
        bool incluiVidros;
        public bool IncluiVidros
        {
            get
            {
                return incluiVidros;
            }
            set
            {
                incluiVidros = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiTravas;
        public bool IncluiTravas
        {
            get
            {
                return incluiTravas;
            }
            set
            {
                incluiTravas = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiAr;
        public bool IncluiAr
        {
            get
            {
                return incluiAr;
            }
            set
            {
                incluiAr = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiCamera;
        public bool IncluiCamera
        {
            get
            {
                return incluiCamera;
            }
            set
            {
                incluiCamera = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiCambio;
        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiSuspensao;
        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiFreios;
        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }
        
        public Veiculo Veiculo { get; set; }

    
        public Decricao (Veiculo veiculo)
		{
			InitializeComponent ();

            this.Title = veiculo.Nome_Do_Modelo;
            this.Veiculo = veiculo;
            this.BindingContext = this;


		}

        private void buttonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(Veiculo));
        }
	}
}