﻿using Senacarros.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacarros.Data
{
    class AgendamentoDAO
    {
        readonly SQLiteConnection Conexao;


        public AgendamentoDAO(SQLiteConnection Conexao)
        {
            this.Conexao = Conexao;
            this.Conexao.CreateTable<Agendamento>();
        }



        public void Salvar(Agendamento agendamento)
        {
            Conexao.Insert(agendamento);
        }
    }
}
